import React from 'react';
import { render } from 'react-dom';
import Hello from './components/Hello';

class App extends React.Component {
  render() {
    return (
      <div>
        <Hello hello={'Hello World!'} />
      </div>
    );
  }
}

render(<App />, document.getElementById('app'));
