import React from 'react';
import PropTypes from 'prop-types';

const Hello = props => <div>{props.hello}</div>;

Hello.propTypes = {
  hello: PropTypes.string
};

export default Hello;
